export function stringify(object) {
    let str = Object.entries(object).reduce((prev, [key, value]) => {
        prev += `${key}=${value};`
        return prev
    }, '')
    return str
}

export function parseString(str) {
    return Object.fromEntries(str.split(';').filter(x => x).map(x => x.split('=')))
}