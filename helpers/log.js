import fs from 'fs';

export default function log() {
  fs.appendFileSync('./logs/system.log', JSON.stringify([Date.now(), ...arguments], 0, 4) + '\n');
  console.log(arguments);
}