import {
    newUserOrder,
    updateUserSetPayment,
} from '../database/dbQueries.js';

import Chance from 'chance'
import TinkoffOplataClient from '@os-utils/tinkoff-oplata'
import bot from '../bot/bot.js';
import cors from 'cors'
import crypto from 'crypto'
import express from 'express'
import log from '../helpers/log.js';

console.log(process.cwd())

// import {sendAfterPaymentMessage} from '../bot/eventHandlers/events/onCallbackQuery.js';
export default async function server() {
    const app = express()
    const TinkoffApi = TinkoffOplataClient.default
    const paymentClient = new TinkoffApi({
        terminalKey: '1598805178874',
        password: 'nng4lxrwbr0pec1a',
    })
    const chance = new Chance()
    
    app.use(express.json())
    app.use(cors())
    
    // app.get('*', (req, res) => res.send('Hello master.'))
    
    app.post('/api/createPaymentForm', async (req, res) => {
        let {amount, chatId, phone} = req.body;
    
        // const result = await paymentClient.addCustomer({
        //     CustomerKey: chatId,
        //     Phone: phone
        // })
            // console.log({amount, chatId, phone});
            let amountInPenny = amount*100
            const chanceStr = chance.string({length: 10});
            let OrderId = `${chatId}_${Date.now()}_${crypto.randomBytes(3).toString('hex')}_${chanceStr})}`
            log({OrderId, len: OrderId.length})
            let ip = '194.67.116.172'
    
            let paymentLink = await paymentClient.init({
                Amount: amountInPenny,
                OrderId,
                Description: 'Оплата Sezam покупки',
                // Recurrent: 'Y',
                // DATA: {
                    // Phone: phone
                // },
                // CustomerKey: chatId,
                NotificationURL: `http://${ip}/api/paymentSuccess`,
            });
    
            log({paymentLink})
            let result = {success: paymentLink.Success}
            if (paymentLink.Success) result.url = paymentLink.PaymentURL  
            res.status(200).json(result)
    })
    
    
    app.post('/api/paymentSuccess', async (req, res) => {
        try {
            log('SUCCESS PAYMENT CALLBACK!!!!!')
            const tinkoffWebhook = req.body;
            log('tinkoffWebhook', {...tinkoffWebhook});
            
            if (tinkoffWebhook.Status == "AUTHORIZED") {
                const rebillId = tinkoffWebhook.RebillId;
                const OrderId = tinkoffWebhook.OrderId;
                const chatId = Number(OrderId.split('_')[0]);
                log('You received chatId', {chatId});
                const dateCreated = (new Date()).toISOString();
                await updateUserSetPayment(chatId, rebillId, dateCreated);
                await newUserOrder(chatId, dateCreated, tinkoffWebhook, 'card');
                await bot.sendMessage(chatId, 'Успешно оплачено!');
                res.status(200).send('OK');
            }
        } catch (error) {
            log({error});
            res.status(500).send('NOT OK');
        }
    })
    
    app.get('/tinkoff/success', (req, res) => {
        res.send('О боже, ты все оплатил, какой ты молодец! Сезам стал богаче!')
    })
    
    
    app.listen(4000, () => log('Server is started'))
        
}
