import db from './postgress.js'

export async function getUserInfo(telegramId) {
    let user = {
        exists: false,
        info: {}
    }
    return db.query("select * from users where telegram_id = $1", [telegramId])
        .then(result => {
            user.exists = result.rowCount > 0 ? true : false
            if (user.exists) user.info = result.rows[0]
            return user
        })
        .catch(error => console.log({message: `User doesn't exist`, ...error}))
}

export async function createUser(telegramId, firstName, lastName, username,) {
    let user = {}
    let cartId
    try {
        let result = await db.query("insert into carts (balance) values ('0') returning cart_id")
        cartId = result.rows[0].cart_id
    } catch(cartIdErr) {
        console.log({cartIdErr})
    }

    if (cartId) {
        try {
            let result = await db.query(
                `insert into users (
                    telegram_id,
                    cart_id,
                    username,
                    first_name,
                    last_name
                ) 
                values ($1, $2, $3, $4, $5) returning *`,
                [telegramId, cartId, username, firstName, lastName]
            )
            user.info = result.rows[0]
        } catch(createUserErr) {
            console.log({createUserErr})
        }
    }
    
    if (user.info) {
        user.exists = true
        return user
    }
}

export async function updateUserTrackItem(telegramId, item) {
    return db.query(`update users set item_last = $1 where telegram_id = $2`, [item, telegramId])
        .then(result => result)
        .catch(updateUserTrackItemErr => console.log(updateUserTrackItemErr))
}

export async function updateUserTrackPeriod(telegramId, period) {
    return db.query(`update users set item_period = $1 where telegram_id = $2`, [period, telegramId])
        .then(result => result)
        .catch(updateUserTrackItemErr => console.log(updateUserTrackItemErr))
}

export async function getUserLastItem(telegramId) {
    return db.query('select item_last from users where telegram_id = $1', [telegramId])
        .then(result => {
            let lastItem = result.rows[0].item_last
            return lastItem
        })
        .catch(error => error)
}

export async function addProductToCart(telegramId, product) {
    console.log({telegramId, product});
    try {
        const cartProduct = await getItemInfoFromUserCart(telegramId, product.product_id);
        if (cartProduct) {
            await db.query(
                `UPDATE cart_products
                SET
                quantity = $1,
                multiplier = $2,
                period = $3
                WHERE telegram_id = $4 AND product_id = $5`,
                [product.quantity, product.multiplier, product.period, telegramId, product.product_id]
            )
        } else {
            await db.query(
                `INSERT INTO cart_products (
                    telegram_id,
                    product_id,
                    quantity,
                    period,
                    multiplier,
                    price
                ) VALUES ($1, $2, $3, $4, $5, $6)`,
                [
                    telegramId,
                    product.product_id,
                    product.quantity,
                    product.period,
                    product.multiplier,
                    product.price
                ]
            )
        }
    } catch(queryErr) {
        throw new Error(queryErr);
    }
}

export async function getItemInfoFromUserCart(telegramId, productId) {
    try {
        console.log({telegramId, productId})
        const queryResult = await db.query(
            `SELECT * FROM cart_products
            WHERE telegram_id = $1 AND product_id = $2`,
            [telegramId, productId]
        );
        console.log(queryResult.rows);
        const cartProduct = queryResult.rows[0];
        return cartProduct;
    } catch (queryErr) {
        throw new Error(queryErr);
    }
}

export async function getCartItems(telegramId) {
    return db.query(`select * from cart_products where telegram_id = $1`, [telegramId])
        .then(result => {
            console.log('getCartItems', {rows: result.rows})
            let exists = result.rowCount > 0 ? true : false
            if (exists) return result.rows
            else return []
        })
        .catch(error => error)
}

export async function queryAllCategories() {
    return db.query(`select * from categories order by name asc`).then(result => {
        return result.rows
    })
}

export async function queryAllCategoryProducts(categoryId) {
    return db.query('select * from products where category_id = $1 order by product_id asc', [categoryId]).then(result => result.rows)
}

export async function getProductInfo(productId) {
    const queryResult = await db.query('select * from products where product_id = $1', [productId])
    return queryResult.rows[0];
}

export async function addProductsToSubscriptionCart(telegramId, products) {
    try {
        const stringProducts = JSON.stringify(products);
        console.log({stringProducts});
        const subscriptionCartQuery = await db.query('select * from order_carts where telegram_id = $1', [telegramId]);
        console.log({subscriptionCartQuery: subscriptionCartQuery.rows});
        const exists = subscriptionCartQuery.rows.length > 0 ? true : false;
        console.log({exists});
        console.log({
            telegramId,
            stringProducts,
            idPaid: false,
        })
        if (!exists) {
            await db.query(`
                INSERT INTO order_carts (telegram_id, is_paid, order_products)
                VALUES ($1, $2, $3)`,
                [telegramId, false, stringProducts]
            );
        } else {
            await db.query('update order_carts set order_products = $1 where telegram_id = $2',
                [stringProducts, telegramId]
            );
        };   
    } catch(queryErr) {
        console.log({queryErr});
        throw new Error(queryErr);
    }
};

export async function updateUserSetPhone(telegramId, phoneNumber) {
    await db.query(
        `UPDATE users
        SET phone = $1 WHERE telegram_id = $2`,
        [phoneNumber, telegramId]
    )
}

export async function updateUserSetPayment(telegramId, rebillId, timestamp) {
    try {
        await db.query(
            `UPDATE users SET
            rebill_id = $1, last_date_paid = $2
            WHERE telegram_id = $3`,
            [rebillId, timestamp, telegramId]
        );
    } catch(queryErr) {
        throw new Error(queryErr);
    }
}

export async function newUserOrder(telegramId, timestamp, providerData, payType) {
    console.log({arguments});
    try {
        const stringProviderData = JSON.stringify(providerData);
        await db.query(
            `INSERT INTO orders (
                telegram_id, provider_data, date_created, pay_type
            ) VALUES ($1, $2, $3, $4)`,
            [telegramId, stringProviderData, timestamp, payType]
        );
    } catch(queryErr) {
        throw new Error(queryErr);
    }
    
}