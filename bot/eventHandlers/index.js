import onCallbackQuery from './events/onCallbackQuery.js';
import onContact from './events/onContact.js';
import onMessage from './events/onMessage.js';
import onPollingError from './events/onPollingError.js';

export {
  onCallbackQuery,
  onContact,
  onMessage,
  onPollingError,
}