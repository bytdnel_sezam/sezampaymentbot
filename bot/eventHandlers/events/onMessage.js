import {
  createUserIfNotExists,
  doActionsOnCustomCommands,
  getImage,
  getSum,
} from '../../functions/index.js';
import {
  getCartItems,
  getUserInfo,
  newUserOrder
} from '../../../database/dbQueries.js';

import axios from 'axios';
import bot from '../../bot.js';
import {keyboardMenu} from '../../keyboards/index.js';

// bot.setMyCommands()
const onMessage = () => {
  bot.on('message', async (msg) => {
    console.log('-------NEW MESSAGE-------');
    console.log(msg);
    console.log('-----END OF MESSAGE-----');
    
    const chatId = msg.chat.id;
    const firstName = msg.from.first_name;
    const lastName = msg.from.last_name;
    const { username } = msg.from;
    
    console.log({ chatId });
    
    await createUserIfNotExists(
      chatId, firstName, lastName, username,
    );

    const isMessageCommand = await doActionsOnCustomCommands(msg)
  
    if (!isMessageCommand) {
      if (msg.text == '/start') {
        console.log(process.cwd());
        bot.sendMessage(chatId, 'Здравствуйте! Я — Sezam-bot и я занимаюсь доставкой продуктов по подписке! Я помогу вам начать экономить от 300 часов в год на покупках. Смотрите: вот, как это работает');
        
        await bot.sendChatAction(chatId, 'upload_photo')
        await bot.sendPhoto(chatId, getImage('/f_000.jpg'))
        await bot.sendPhoto(chatId, getImage('/f_00_1.jpg'))
        await bot.sendPhoto(chatId, getImage('/f_000_2.jpg'))
        await bot.sendMessage(
          chatId,
          `
          Надеюсь, что вы разобрались! 😂 
          Теперь вперёд - смотреть товары.
          Если возникли вопросы — нажмите на кнопку «задать вопрос»
          `,
          {
            reply_markup: {
              inline_keyboard: keyboardMenu
            }
          }
        )
      } else if (msg.contact) {
        // bot.sendMessage(chatId, '', {reply_markup: {remove_keyboard: true}});
        bot.sendMessage(chatId, 'Номер телефона записан.');
      } else if (msg.text == 'Оплата картой') {
        const userInfo = await getUserInfo(chatId);
        
        console.log('###### BEFORE PAYMENT LINK ######');
        const {
          username,
          last_name,
          first_name,
          // email,
          delivery_address,
          phone
        } = userInfo.info;
        
        let infoText = `Username: @${username}`;
        infoText += `\nФамилия: ${last_name}`;
        infoText += `\nИмя: ${first_name}`;
        // infoText += `\nEmail: ${email || 'Не указано ⚠️'}`
        infoText += `\nАдрес доставки для подписки: ${delivery_address || 'Не указано ⚠️'}`;
        infoText += `\nНомер телефона: ${phone || 'Не указано ⚠️'}`;
        await bot.sendMessage(chatId, infoText);
        // if (!email) {
        //   let emailWarningText = `⚠️ Email не указан.`
        //   emailWarningText += `\nОтправьте боту команду /email [email]`
        //   emailWarningText += `\nНапример /email example@mymail.ru`
        //   bot.sendMessage(chatId, emailWarningText);
        // }
        if (!delivery_address) {
          let deliveryAddressWarningText = `⚠️ Адрес доставки не указан.`
          deliveryAddressWarningText += `\nОтправьте боту команду /delivery_address [delivery_address]`
          deliveryAddressWarningText += `\nНапример /delivery_address Москва, ул. Маяковского, д. 24к2 кв. 134`
          bot.sendMessage(chatId, deliveryAddressWarningText);
        }
        if (!phone) {
          let phoneWarningText = `⚠️ Номер телефона не указан.`
          phoneWarningText += `\nНажми на кнопку "Предоставить номер телефона"`
          bot.sendMessage(chatId, phoneWarningText);
        }
  
        console.log('###### PAYMENT CHECKPOINT LINK ######');
  
        const allDataFull = phone && delivery_address;
        if (allDataFull) {
          const amount = await getSum(chatId);
          const ip = '194.67.116.172';
          const userInfo = await getUserInfo(chatId);
          const message = 'Доступна оплата через:';
          
          // console.log({ userInfo });
          
          try {
            const response = (await axios.post(`http://${ip}/api/createPaymentForm`, {
              chatId,
              amount,
              phone: userInfo.info.phone,
            })).data;
  
            bot.sendMessage(
              chatId,
              'Все данные для оформления заказа у нас есть.',
              {reply_markup: {remove_keyboard: true}}
            );
            
            bot.sendMessage(chatId, message, {
              reply_markup: {inline_keyboard: [[{text: 'Tinkoff Оплата', url: response.url}]]}});
          } catch (e) {
            console.log({message: e.message});
            bot.sendMessage(chatId, 'Не удалось получить ссылку на оплату');
        }
        }
      } else if (msg.text == 'Оплата наличными при доставке') {
        const userInfo = await getUserInfo(chatId);
        
        console.log('###### BEFORE PAYMENT LINK ######');
        const {
          username,
          last_name,
          first_name,
          // email,
          delivery_address,
          phone
        } = userInfo.info;
        
        let infoText = `Username: @${username}`;
        infoText += `\nФамилия: ${last_name}`;
        infoText += `\nИмя: ${first_name}`;
        // infoText += `\nEmail: ${email || 'Не указано ⚠️'}`
        infoText += `\nАдрес доставки для подписки: ${delivery_address || 'Не указано ⚠️'}`;
        infoText += `\nНомер телефона: ${phone || 'Не указано ⚠️'}`;
        await bot.sendMessage(chatId, infoText);
        // if (!email) {
        //   let emailWarningText = `⚠️ Email не указан.`
        //   emailWarningText += `\nОтправьте боту команду /email [email]`
        //   emailWarningText += `\nНапример /email example@mymail.ru`
        //   bot.sendMessage(chatId, emailWarningText);
        // }
        if (!delivery_address) {
          let deliveryAddressWarningText = `⚠️ Адрес доставки не указан.`
          deliveryAddressWarningText += `\nОтправьте боту команду /delivery_address [delivery_address]`
          deliveryAddressWarningText += `\nНапример /delivery_address Москва, ул. Маяковского, д. 24к2 кв. 134`
          bot.sendMessage(chatId, deliveryAddressWarningText);
        }
        if (!phone) {
          let phoneWarningText = `⚠️ Номер телефона не указан.`
          phoneWarningText += `\nНажми на кнопку "Предоставить номер телефона"`
          bot.sendMessage(chatId, phoneWarningText);
        }
  
        console.log('###### PAYMENT CHECKPOINT LINK ######');
  
        const allDataFull = phone && delivery_address;

        if (allDataFull) {
          await bot.sendMessage(
            chatId,
            'Все данные для оформления заказа у нас есть.',
            {reply_markup: {remove_keyboard: true}}
          );
          const dateCreated = (new Date()).toISOString();
          const cartItems = await getCartItems(chatId);
          
          try {
            await newUserOrder(chatId, dateCreated, cartItems, 'cash');
            await bot.sendMessage(chatId, 'Вы заказали продукты. В скором времени с вами свяжутся и привезут заказ.');
          } catch(onDublicateError) {
            await bot.sendMessage(chatId, 'Похоже, что-то пошло не так. Вы уже оформили такой заказ');
          }
        }
      } else {
        await bot.sendMessage(chatId, 'Если возникли вопросы - нажмите на кнопку «задать вопрос»', {
          reply_markup: {
            inline_keyboard: keyboardMenu,
          },
        });
      } 
    }
  }); 
}

export default onMessage;