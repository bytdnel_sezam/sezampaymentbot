import bot from '../../bot.js';

const onPollingError = () => {
  bot.on('polling_error', (error) => {
    console.log({ error })
  });  
}

export default onPollingError;
