import bot from '../../bot.js';
import {setUserPhone} from '../../functions/index.js';

const onContact = () => {
  bot.on('contact', async (msg) => {
    const chatId = msg.chat.id;
    const phoneNumber = msg.contact.phone_number;
    await setUserPhone(chatId, phoneNumber);
  });
}

export default onContact;
