import {
  addProductToCart,
  addProductsToSubscriptionCart,
  getCartItems,
} from '../../../database/dbQueries.js';
import {
  createCategoriesKeyboard,
  createProductsKeyboard,
  getCartItemInfo,
  getCartProducts,
  getCategories,
  getCategoryProducts,
  getImage,
  getItemName,
  getItemPrice,
  getLast,
  getProductInfoText,
  getSum,
  keyboardCartDecorator,
  keyboardCatalogueDecorator,
  trackItem,
  trackPeriod,
} from '../../functions/index.js';
import {
  keyboardCounter,
  keyboardPeriod
} from '../../keyboards/index.js'
import {parseString, stringify} from '../../../helpers/queryFormatter.js';

import axios from 'axios';
import bot from '../../bot.js';

const dataByPeriod = {
  p1: { x: 14, p: '2 дня' },
  p2: { x: 4, p: '7 дней' },
  p3: { x: 2, p: '14 дней' },
  p4: { x: 1, p: '28 дней' },
};

const onCallbackQuery = () => {
  bot.on('callback_query', async (query) => {
    const chatId = query.message.chat.id;
    const parsedQueryData = parseString(query.data);
    
    console.log({parsedQueryData});
  
    switch (parsedQueryData.type) {
      case 'catalogue':
        {
          const categories = await getCategories();
          const categoriesKeyboard = keyboardCartDecorator(createCategoriesKeyboard(categories));
          console.log({ categoriesKeyboard });
          bot.sendMessage(chatId, 'Каталог товаров:', {
            reply_markup: {
              inline_keyboard: categoriesKeyboard,
            },
          });
        }
        break;
      case 'category':
        {
          console.log({parsedQueryData});
          const imgBuffer = getImage(parsedQueryData.img_path);
          const products = await getCategoryProducts(parsedQueryData.category_id);
          
          console.log({ products });

          const productsKeyboard = keyboardCatalogueDecorator(keyboardCartDecorator(createProductsKeyboard(products)));

          bot.sendPhoto(chatId, imgBuffer, {
            reply_markup: {
              inline_keyboard: productsKeyboard,
            },
          });
        }
        break;
      case 'product':
        {
          console.log({ parsedQueryData });
          
          const { product_id } = parsedQueryData;
          trackItem(chatId, product_id).then(async () => {
            await bot.sendMessage(chatId, `Выберите периодичность, с которой вы хотите получать товар "${await getItemName(product_id)}".`, {
              reply_markup: {
                inline_keyboard: keyboardPeriod,
              },
            });
          });
        }
        break;
      case 'add':
        {
          console.log('ADD');
          
          const product_id = await getLast(chatId);
          
          console.log({ product_id });

          const cartProductInfo = await getCartItemInfo(chatId, product_id);

          console.log({ cartProductInfo });
          
          const name = await getItemName(product_id);
          const quantity = cartProductInfo.quantity + 1;
          const price = await getItemPrice(product_id);
          const { multiplier } = cartProductInfo;
          const { period } = cartProductInfo;
          const product = {
            product_id, name, quantity, price, multiplier, period,
          };
          console.log({ product });
          await addProductToCart(chatId, product);
          await bot.sendMessage(chatId, getProductInfoText(product), { reply_markup: { inline_keyboard: keyboardCounter } });
        }
        break;
      case 'reduce':
        {
          console.log('REDUCE');
          const product_id = await getLast(chatId);
          const cartProductInfo = await getCartItemInfo(chatId, product_id);
          const name = await getItemName(product_id);
          let quantity = cartProductInfo.quantity - 1;
          if (quantity < 0) quantity = 0;
          const price = await getItemPrice(product_id);
          const { multiplier } = cartProductInfo;
          const { period } = cartProductInfo;
          const product = {
            product_id, name, quantity, price, multiplier, period,
          };
          console.log({ product });
          await addProductToCart(chatId, product);
          await bot.sendMessage(chatId, getProductInfoText(product), { reply_markup: { inline_keyboard: keyboardCounter } });
        }
        break;
      case 'cart':
        {
          const keyboardCart = []
          const catalogueButton = [{text: '🧾 В каталог', callback_data: stringify({type: 'catalogue'})}]
          const subscriptionButton = [{text: 'Оформление подписки', callback_data: stringify({type: 'order'})}]
          const cartProducts = await getCartProducts(chatId);
          console.log('########## CART ##########');
          console.log(cartProducts);
          console.log('########## CART END ##########')
          if (cartProducts.length) {
            // let cartProductsInfo = await getCartItemsInfo(cartProducts);
            // console.log({cartProductsInfo});
            for (const product of cartProducts) {
              const fullPrice = product.quantity * product.multiplier * product.price;
              const text = `${await getItemName(product.product_id)} [${fullPrice}₽]`;
              keyboardCart.push([{text, callback_data: stringify({type: 'product', product_id: product.product_id})}]); 
            }
            keyboardCart.push(subscriptionButton, catalogueButton);
            const cartSum = await getSum(chatId);
            bot.sendMessage(chatId, `Сумма вашей корзины: ${cartSum}₽`, { reply_markup: { inline_keyboard: keyboardCart } });
          } else {
            console.log('there is no cart products!!!!!!')
            keyboardCart.push(catalogueButton);
            console.log(keyboardCart);
            bot.sendMessage(chatId, 'В вашей корзине нет товаров', { reply_markup: { inline_keyboard: keyboardCart } });
          }
        }
        break;
      case 'payment':
        {
          console.log(query);
          const amount = await getSum(chatId);
          const ip = '194.67.116.172';
          const response = (await axios.post(`http://${ip}/api/createPaymentForm`, { chatId, amount })).data;
          let message = '';
          if (response.success) message = response.url;
          else message = 'Извините, произошла какая-то ошибка. Напишите по поводу оплаты товара @shakildanil';
          bot.sendMessage(chatId, message);
        }
        break;
      case 'period':
        {
          console.log('period');
          console.log({ parsedQueryData });
          console.log(dataByPeriod[parsedQueryData.period]);
          const { x, p } = dataByPeriod[parsedQueryData.period];
          await trackPeriod(chatId, x);
          const lastItem = await getLast(chatId);
          const multiplier = x;
          const period = p;
          const product = {
            product_id: lastItem,
            name: await getItemName(lastItem),
            quantity: 0,
            period,
            multiplier,
            price: await getItemPrice(lastItem),
          };
          console.log({ product });
          await addProductToCart(chatId, product);
          bot.sendMessage(chatId, getProductInfoText(product), { reply_markup: { inline_keyboard: keyboardCounter } });
        }
        break;
      case 'order':
        {
          const cartProducts = await getCartItems(chatId);
          console.log({ cartProducts });
          await addProductsToSubscriptionCart(chatId, cartProducts);
          const paymentKeyboard = [[{
            text: 'Оплатить',
            callback_data: stringify({ type: 'payment' }),
            // request_contact: true,
          }]];
          const replyKeyboard = keyboardCartDecorator(keyboardCatalogueDecorator(paymentKeyboard));
          // bot.sendMessage(
          //     chatId,
          //     'JSON.stringify(cartProducts)',
          //     {
          //         reply_markup: {inline_keyboard: replyKeyboard}
          //     }
          // );
          bot.sendMessage(chatId, 'Введите все данные и оплачивайте заказ!', {
            reply_markup: {
              keyboard: [[
                {
                  text: 'Предоставить номер телефона',
                  request_contact: true,
                },
                {
                  text: 'Оплата картой',
                },
                {
                  text: 'Оплата наличными при доставке'
                }
              ]],
  
            },
  
          });
        }
        break;
      default:
        {
          console.log('Такого кейса нет!');
          console.log({
            query: query.data,
            parsed: parseString(query.data),
          });
        }
        break;
    }
  });
};

export default onCallbackQuery;
