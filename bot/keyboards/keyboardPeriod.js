import { stringify } from '../../helpers/queryFormatter.js';

const keyboardPeriod = [
  // [
  //   {
  //     text: 'Раз в 2 дня',
  //     callback_data: stringify({type: 'period', period: 'p1'})
  //   },
  // ],
  [
    {
      text: '1️⃣ Раз в 1 неделю',
      callback_data: stringify({ type: 'period', period: 'p2' }),
    },
  ],
  [
    {
      text: '2️⃣ Раз в 2 недели',
      callback_data: stringify({ type: 'period', period: 'p3' }),
    },
  ],
  [
    {
      text: '3️⃣ Раз в месяц',
      callback_data: stringify({ type: 'period', period: 'p4' }),
    },
  ],
  [
    {
      text: '🧀 В каталог',
      callback_data: stringify({ type: 'catalogue' }),
    },
  ],
];

export default keyboardPeriod;
