import { stringify } from '../../helpers/queryFormatter.js';

const keyboardCounter = [
  [
    {
      text: '+1',
      callback_data: stringify({ type: 'add' }),
    },
    {
      text: '-1',
      callback_data: stringify({ type: 'reduce' }),
    },
  ],
  [
    {
      text: '🧾 В каталог',
      callback_data: stringify({ type: 'catalogue' }),
    },
  ],
  [
    {
      text: '📦 Перейти в корзину',
      callback_data: stringify({ type: 'cart' }),
    },
  ],

];

export default keyboardCounter;
