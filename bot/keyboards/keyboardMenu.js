import { stringify } from '../../helpers/queryFormatter.js';

const keyboardMenu = [
  [
    {
      text: '🧾 Каталог товаров',
      callback_data: stringify({ type: 'catalogue' }),
    },
  ],
  [
    {
      text: '📦 Перейти в корзину',
      callback_data: stringify({ type: 'cart' }),
    },
  ],
  [
    {
      text: '❔ Задать вопрос поддержке',
      callback_data: stringify({ type: 'question' }),
      url: 'https://t.me/sezam_help',
    },
  ],
];

export default keyboardMenu;
