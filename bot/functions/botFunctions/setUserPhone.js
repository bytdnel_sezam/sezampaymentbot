import {updateUserSetPhone} from '../../../database/dbQueries.js';

export default async function setUserPhone(telegramId, phoneNumber) {
    await updateUserSetPhone(telegramId, phoneNumber);
}