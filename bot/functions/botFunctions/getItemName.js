import {getProductInfo} from '../../../database/dbQueries.js';

export default async function getItemName(productId) {
  console.log('getItemName', { productId });
  const productInfo = await getProductInfo(productId);
  return productInfo.name;
}