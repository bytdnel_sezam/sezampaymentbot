import {getItemInfoFromUserCart} from '../../../database/dbQueries.js';

export default async function getCartItemInfo(chatId, productId) {
  return await getItemInfoFromUserCart(chatId, productId);
}