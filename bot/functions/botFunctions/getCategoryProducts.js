import {queryAllCategoryProducts} from '../../../database/dbQueries.js';

export default async function getCategoryProducts(categoryId) {
  return await queryAllCategoryProducts(categoryId);
}