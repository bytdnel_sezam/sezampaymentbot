import db from '../../../database/postgress.js';

export default async function setUserDeliveryAddress(telegramId, deliveryAddress) {
  try {
    await db.query(
      `UPDATE users
      SET delivery_address = $1
      WHERE telegram_id = $2`,
      [deliveryAddress, telegramId]
    )
  } catch (queryErr) {
    throw new Error(queryErr);
  }
}