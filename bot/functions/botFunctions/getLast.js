import {getUserLastItem} from '../../../database/dbQueries.js';

export default async function getLast(chatId) {
  const lastItem = await getUserLastItem(chatId);
  return lastItem;
}