import {updateUserTrackItem} from '../../../database/dbQueries.js';

export default async function trackItem(chatId, productId) {
  await updateUserTrackItem(chatId, productId);
}