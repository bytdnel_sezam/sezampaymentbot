import {
  getCartItems,
} from '../../../database/dbQueries.js';
import getItemName from './getItemName.js';
import {stringify} from '../../../helpers/queryFormatter.js';

export default async function getCartProducts(chatId) {
  const cartProducts = await getCartItems(chatId);
  console.log({CartLength: cartProducts.length})
  if (!cartProducts) {
    return null;
  } else {
    let onlyQuantityNotEmpty = cartProducts.filter(product => product.quantity !== 0);
    return onlyQuantityNotEmpty;
  }
  
  // for (const product of cartItems) {
  //   const fullPrice = product.quantity * product.multiplier * product.price;
  //   if (fullPrice) {
  //     const text = `${await getItemName(product.product_id)} [${fullPrice}р]`;
  //     keyboard.unshift([{text, callback_data: stringify({type: 'product', product_id: product.product_id})}]);
  //   }
  // }
}