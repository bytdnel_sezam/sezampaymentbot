// /address [address]
import bot from '../../bot.js';
import setUserDeliveryAddress from './setUserDeliveryAddress.js'

export default async function doActionsOnCustomCommands(telegramMessage) {
  const chatId = telegramMessage.chat.id;
  const userText = telegramMessage.text;

  if (userText) {
    try {
      if (userText.includes('/delivery_address ')) {
        let [_empty, address] = userText.split('/delivery_address ');
        address.trim();
        if (!address) bot.sendMessage(chatId, 'Вы что-то сделали не так!');
        else {
          // save to db
          console.log('А потом сохранить данные в бд');
          await setUserDeliveryAddress(chatId, address);
        }
        console.log({_empty, address});
        await bot.sendMessage(chatId, 'Адрес доставки для этого заказа сохранен.');
        return true;
      }
    } catch (error) {
      console.log({error});
      bot.sendMessage(chatId, 'Возникла какая-то ошибка, попробуйте снова.');
    }  
  }  
}