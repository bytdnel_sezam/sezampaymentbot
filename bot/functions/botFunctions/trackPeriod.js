import {updateUserTrackPeriod} from '../../../database/dbQueries.js';

export default async function trackPeriod(chatId, period) {
  await updateUserTrackPeriod(chatId, period);
}