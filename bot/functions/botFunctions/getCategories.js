import {queryAllCategories} from '../../../database/dbQueries.js';

export default async function getCategories() {
  return await queryAllCategories();
}