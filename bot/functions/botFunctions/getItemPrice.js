import {getProductInfo} from '../../../database/dbQueries.js';

export default async function getItemPrice(productId) {
  console.log('getItemPrice', { productId });
  const productInfo = await getProductInfo(productId);
  return productInfo.price;
}