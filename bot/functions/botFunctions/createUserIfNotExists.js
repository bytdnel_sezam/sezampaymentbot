import {
  createUser,
  getUserInfo
} from '../../../database/dbQueries.js';

export default async function createUserIfNotExists(chatId, firstName, lastName, username) {
  let user = await getUserInfo(chatId);
  console.log({ user });
  if (!user.exists) user = await createUser(chatId, firstName, lastName, username);
}