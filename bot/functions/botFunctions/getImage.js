import fs from 'fs';

export default function getImage(imageFile) {
  console.log({imageFile});
  const ImagesPath = '/images';
  return fs.readFileSync(`.${ImagesPath}${imageFile}`);
}