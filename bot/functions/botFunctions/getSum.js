import {getCartItems} from '../../../database/dbQueries.js';
import getItemPrice from './getItemPrice.js';

export default async function getSum(chatId) {
  const cartItems = await getCartItems(chatId);
  console.log('getSum', { cartItems });
  let cartSum = 0;
  for (const product of cartItems) {
    // const price = await getItemPrice(product.product_id);
    const { quantity, price, multiplier } = product;
    console.log('counting sum', {product});
    cartSum += (price * quantity * multiplier);
  }
  console.log({ cartSum });
  return cartSum;
}