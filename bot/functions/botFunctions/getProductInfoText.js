export default function getProductInfoText(product) {
  const message = `Супер🔥 В корзине сейчас ${product.quantity} шт. "${product.name}"\n`
    + `Нажмите +1, чтобы увеличить кол-во, или -1, чтобы уменьшить.\n`
    + `Стоимость 1 единицы товара - ${product.price}₽.\n`
  return message;
}