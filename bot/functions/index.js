import createCategoriesKeyboard from './keyboardFunctions/createCategoriesKeyboard.js';
import createProductsKeyboard from './keyboardFunctions/createProductsKeyboard.js';
import createUserIfNotExists from './botFunctions/createUserIfNotExists.js';
import doActionsOnCustomCommands from './botFunctions/doActionsOnCustomCommands.js';
import getCartItemInfo from './botFunctions/getCartItemInfo.js';
// import getCartItemsInfo from './botFunctions/getCartItemsInfo.js';
import getCartProducts from './botFunctions/getCartProducts.js';
import getCategories from './botFunctions/getCategories.js';
import getCategoryProducts from './botFunctions/getCategoryProducts.js';
import getImage from './botFunctions/getImage.js';
import getItemName from './botFunctions/getItemName.js';
import getItemPrice from './botFunctions/getItemPrice.js';
import getLast from './botFunctions/getLast.js';
import getProductInfoText from './botFunctions/getProductInfoText.js';
import getSum from './botFunctions/getSum.js';
import keyboardCartDecorator from './keyboardFunctions/keyboardCartDecorator.js';
import keyboardCatalogueDecorator from './keyboardFunctions/keyboardCatalogueDecorator.js';
import setUserPhone from './botFunctions/setUserPhone.js';
import trackItem from './botFunctions/trackItem.js';
import trackPeriod from './botFunctions/trackPeriod.js';

export {
  createCategoriesKeyboard,
  createProductsKeyboard,
  createUserIfNotExists,
  doActionsOnCustomCommands,
  getCartProducts,
  // getCartItemsInfo,
  getCartItemInfo,
  getCategories,
  getCategoryProducts,
  getImage,
  getItemName,
  getItemPrice,
  getLast,
  getProductInfoText,
  getSum,
  keyboardCartDecorator,
  keyboardCatalogueDecorator,
  setUserPhone,
  trackItem,
  trackPeriod,
}
