import {stringify} from '../../../helpers/queryFormatter.js';

export default function keyboardCartDecorator(keyboard) {
  keyboard.push([
    {
      text: '📦 Перейти в корзину',
      callback_data: stringify({ type: 'cart' }),
    },
  ]);
  return keyboard;
}