import {stringify} from '../../../helpers/queryFormatter.js';

export default function createProductsKeyboard(array) {
  return array.map((product) => [
    {
      text: `${product.name} - ${product.price}₽`,
      callback_data: stringify({
        type: 'product',
        product_id: product.product_id,
      }),
    },
  ]);
}