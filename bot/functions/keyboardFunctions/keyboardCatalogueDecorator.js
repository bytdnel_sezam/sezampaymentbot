import {stringify} from '../../../helpers/queryFormatter.js';

export default function keyboardCatalogueDecorator(keyboard) {
  keyboard.push([
    {
      text: '🧾 Перейти в каталог',
      callback_data: stringify({ type: 'catalogue' }),
    },
  ]);
  return keyboard;
}