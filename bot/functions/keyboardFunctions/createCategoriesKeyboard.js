import {stringify} from '../../../helpers/queryFormatter.js';

export default function createCategoriesKeyboard(array) {
  return array.map((category) => [
    {
      text: category.name,
      callback_data: stringify({
        type: 'category',
        category_id: category.category_id,
        img_path: category.img_path,
      }),
    },
  ]);
}