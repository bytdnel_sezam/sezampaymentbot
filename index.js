import * as botEventHandlers from './bot/eventHandlers/index.js';

// import * as functions from './functions/index.js';
// import * as keyboards from './keyboards/index.js';
import server from './server/server.js';

// define all event handlers of type bot.on('eventHandler')

(async function () {
  await server();
  
  Object.values(botEventHandlers)
    .forEach(eventHandler => eventHandler());
})();


